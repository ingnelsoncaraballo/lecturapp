import React, { Component } from "react";
import { StyleProvider } from "native-base";

import Setup from "./src/setup";
import getTheme from "./src/theme/components";
import variables from "./src/theme/variables/material";
import SyncStorage from 'sync-storage';

export default class App extends Component {

  async UNSAFE_componentWillMount (): void { 
    const data = await SyncStorage.init ();  
  }

  render() {
    return (
      <StyleProvider style={getTheme(variables)}>
        <Setup />
      </StyleProvider>
    );
  }
}