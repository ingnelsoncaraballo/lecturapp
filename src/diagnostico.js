import React, { Component } from "react";
import { Image, Dimensions } from "react-native";
import { Container, Header, Title, Content, Button, Icon, Card, CardItem, Text, Thumbnail, Left, Body, Right } from "native-base";
import HTML from 'react-native-render-html';
import YoutubePlayer from 'react-native-youtube-iframe';
import styles from "./styles/styles";

const image = require("../img/diagnostico.jpg");

class Diagnostico extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            lectura: 'Roberto era un niño al que solo le gustaba que le regalaran videojuegos, aparatos electrónicos, coches teledirigidos y cualquier regalo tecnológico que fuera muy caro. Siempre que tenía que elegir algo por su cumpleaños o escribía la carta a los Reyes Magos miraba primero el precio de lo que quería. <br> Un día, en la víspera de su cumpleaños, mientras dormía, Roberto tuvo un sueño realmente extraño. Soñó que él y su familia se mudaban a un extraño país llamado El País de los regalos raros. <br> En su sueño, Roberto se despertaba rodeado de pequeños paquetes y empezaba a abrirlos todos. <br> - Seguro que esta caja contiene un teléfono móvil o una videoconsola nueva -dijo Roberto.  <br>Pero cuando abrió la caja se encontró un extraño objeto rectangular, lleno de dibujos extraños y colores llamativos que, al abrirse, contenía cientos y cientos de finos objetos también llenos de símbolos y colores. <br>- ¿Qué será esto? -se preguntó Roberto. <br>Por más que lo miraba no conseguía saber para qué servía, así que abrió otro paquete. El contenido era muy parecido, solo que un poco más grande. De modo que seguía y seguía abriendo paquetes sin descanso, pero en todos había siempre lo mismo. <br>Empapado en sudor, Roberto se despertó como si de una pesadilla se tratase. <br>Desorientado, puso su habitación patas arriba intentando encontrar algo parecido a los objetos con los que había soñado, pero no encontró ninguno. Así que el muchacho volvió a meterse en la cama hasta que amaneció.<br>Por la mañana Roberto bajó a desayunar. Su familia le esperaba con un suculento desayuno y un gran paquete en la mesa. Roberto sintió que un sudor frío le invadía por todo el cuerpo. <br>Temblando, Roberto abrió el paquete. Dentro había un montón de objetos como los que había visto en su sueño, todos del mismo tamaño. Tenían dibujos de animales, plantas, planetas y otro montón de cosas que a Roberto le eran familiares.<br>- ¿Qué es esto? -preguntó Roberto, confundido.<br>- ¡La enciclopedia más cara del mundo! -gritaron sus padres a coro. <br>- ¿Son libros? -dijo el niño.<br>- Claro que son libros -dijo su padre-. Pero también hay vídeos, material interactivo y juegos para que disfrutes aprendiendo muchas cosas. <br>- ¡Uff! Por un momento pensé que me había vuelto loco... -dijo Roberto, aliviado.<br>Desde ese día, Roberto sueña cosas increíbles que aprende mientras juega con su nueva enciclopedia. ¡Es increíble la cantidad de cosas que se pueden encontrar en los libros!'
        };
    }

    test(){
        this.props.navigation.navigate('TestDiagnostico');
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header>
                    <Body>
                        <Title>Test Diagnostico - El país de los regalos raros</Title>
                    </Body>
                </Header>

                <Content padder>
                    <Card style={styles.mb}>
                        <CardItem cardBody>
                            <Image style={{ resizeMode: "cover", width: null, height: 250, flex: 1 }} source={ image } />
                        </CardItem>

                        <CardItem style={{ paddingVertical: 0 }}>
                            <Body>
                                <HTML html={this.state.lectura} imagesMaxWidth={Dimensions.get('window').width} />
                            </Body>
                        </CardItem>

                        <CardItem>
                            
                            <Body>
                                <Button block success onPress={ () => this.test() }>
                                    <Icon name="md-checkbox-outline" />
                                    <Text>Realizar Test</Text>
                                </Button>
                            </Body>
                            
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default Diagnostico;