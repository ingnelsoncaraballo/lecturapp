import React, { Component } from 'react';
import { Container, Header, Content, Button, List, ListItem, Left, Body, Right, Separator, Text, Title, Icon, Card, CardItem, Thumbnail, Image } from 'native-base';
import ProgressBar from 'react-native-progress/Bar';
import styles from "./styles/styles";
import Axios from 'axios';
import { set } from 'react-native-reanimated';


class Tests extends Component {

    constructor(props){
        super(props);
        this.state = {
            misLecturas: [],
            misRespuestas: [],
            progress: 10
        }
    }

    componentDidMount(){
        if(global.user.role == 0){
            var id = global.user.id;
        }else{
            var id = this.props.navigation.getParam('user_id');
        }
        Axios.post(global.url + 'tests', {
            id: id
        })
        .then(response => {
            var lecturas = response.data;
            var lectura = [];
            var item = 1;
            var progress = parseFloat(0);
            for(var i in lecturas){
                if(this.find(lectura, lecturas[i].readings)){
                    lectura[item - 1] = {
                        'title': lecturas[i].readings,
                        'promedio': lecturas[i].promedio,
                        'image': lecturas[i].image,
                        'bibliography': lecturas[i].bibliography,
                    };
                    item = item + 1;
                    progress = progress + parseFloat(lecturas[i].promedio);
                }
            }
            this.setState({
                misLecturas: lectura,
                misRespuestas: lecturas,
                progress: progress/item/10               
            });
        })
        .catch(function(error){
            console.log(error);
        });
    }

    find(lectura, texto){
        for(var it in lectura){
            if(lectura[it].title == texto)
                return false;
        }
        return true;
    }

    text(){
        if(this.state.misLecturas.length == 0){
            return (
                <Text style={{ margin: 15 }}>Cuando empieces a realizar tus test, aqui podrás observar tus resultados!</Text>
            )
        }
    }

    _lecturas(){
        return (
            this.state.misLecturas.map( (item, key) =>
                <ListItem avatar button onPress={ () => this.props.navigation.navigate('Respuestas', {
                    "respuestas": this.state.misRespuestas,
                    "title": item.title
                }) }
                key={key}>
                    <Left>
                        <Thumbnail source={{ uri: item.image }} />
                    </Left>
                    <Body>
                        <Text>{ item.title }</Text>
                        <Text note>{ item.bibliography }</Text>
                    </Body>
                    <Right>
                        <Text note>{ item.promedio }</Text>
                    </Right>
                </ListItem>  
            )
        )
    }

    render() {
        return (
        <Container>
            <Header>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.goBack()}>
                        <Icon name="arrow-back" />
                    </Button>
                </Left>
                <Body>
                    <Title>Mis Tests</Title>
                </Body>
                <Right />
            </Header>
            <Content>
                <Card style={styles.mb}>
                    <CardItem header bordered style={ styles.center }>
                        <Text>Mi Progreso</Text>
                    </CardItem>
                    <CardItem cardBody style={[{ margin: 10}, styles.center]}>                    
                        <ProgressBar progress={ this.state.progress } width={200} borderRadius={5} />                    
                    </CardItem>
                </Card>
                <Separator bordered>
                </Separator>
                { this.text() }
                <List>
                    { this._lecturas() }
                </List>
            </Content>
        </Container>
        );
    }
}

export default Tests;