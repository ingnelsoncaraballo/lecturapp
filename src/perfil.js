import React, { Component } from 'react';
import { Dimensions, Image } from "react-native";
import { Container, Header, Content, Body, Text, Left, Icon, Separator, ListItem, Button, Title, Right } from 'native-base';
import styles from "./styles/styles";

class Perfil extends Component {
    constructor(props) {
        super(props);
        this.state = {
            perfil: 'https://library.kissclipart.com/20181001/wbw/kissclipart-gsmnet-ro-clipart-computer-icons-user-avatar-4898c5072537d6e2.png',
            rol: global.user.role == 0 ? "Estudiante" : "Profesor"
        };
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Mi Perfil</Title>
                    </Body>
                </Header>
                <Content>
                    <Image source={{ uri: this.state.perfil }} style={[styles.drawerCover, { height: 250 } ]} />
                    <Separator bordered noTopBorder />

                    <ListItem icon>
                        <Body>
                            <Text>Nombre</Text>
                        </Body>
                        <Right>
                            <Text>{global.user.name} { global.user.lastname }</Text>
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Body>
                            <Text>Email</Text>
                        </Body>
                        <Right>
                            <Text>{global.user.email}</Text>
                        </Right>
                    </ListItem>

                    <ListItem icon>
                        <Body>
                            <Text>Rol</Text>
                        </Body>
                        <Right>
                            <Text>{this.state.rol}</Text>
                        </Right>
                    </ListItem>

                    <ListItem icon>
                        <Body>
                            <Text>Edad</Text>
                        </Body>
                        <Right>
                            <Text>{global.user.age}</Text>
                        </Right>
                    </ListItem>
                </Content>
            </Container>
        );
    }
}

export default Perfil;