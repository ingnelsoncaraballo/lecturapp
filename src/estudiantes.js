import React, { Component } from 'react';
import { Container, Header, Content, Button, List, ListItem, Left, Body, Right, Separator, Text, Title, Icon, Card, CardItem, Thumbnail, Image } from 'native-base';
import ProgressBar from 'react-native-progress/Bar';
import styles from "./styles/styles";
import Axios from 'axios';
import { set } from 'react-native-reanimated';
import { ScrollView,  RefreshControl } from 'react-native';


class Estudiantes extends Component {

    constructor(props){
        super(props);
        this.state = {
            refresh: false,
            perfil: 'https://library.kissclipart.com/20181001/wbw/kissclipart-gsmnet-ro-clipart-computer-icons-user-avatar-4898c5072537d6e2.png',
            misEstudiantes: []
        }

        this.loadStudents();  
    }

    loadStudents(){
        Axios.post(global.url + 'testsProfesor', {
            id: global.user.id
        })
        .then(response => {
            var estudiantes = response.data.estudiantes;
            this.setState({
                misEstudiantes: estudiantes,               
            });           
        })
        .catch(function(error){
            console.log(error);
        });
    }

    _estudiantes(){
        return (
            this.state.misEstudiantes.map( (item, key) =>
                <ListItem avatar button onPress={ () => this.props.navigation.navigate('Tests', {
                    "user_id": item.id
                }) }
                key={key}>
                    <Left>
                        <Thumbnail source={{ uri: this.state.perfil }} />
                    </Left>
                    <Body>
                        <Text>{ item.name } { item.lastname }</Text>
                        <Text note>Edad: { item.age}, Nivel: { item.nivel }, Email: { item.email } </Text>
                    </Body>
                    <Right>
                        <Text note>{ item.promedio }</Text>
                    </Right>
                </ListItem>  
            )
        )
    }

    _refresh(){
        this.loadStudents();
    }

    render() {
        return (
        <Container>
            <Header>
                <Left>
                    <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                        <Icon name="menu" />
                    </Button>
                </Left>
                <Body>
                    <Title>Mis Estudiantes</Title>
                </Body>
                <Right />
            </Header>
            <Content>
            <ScrollView
                refreshControl={
                    <RefreshControl
                        refreshing={ this.state.refresh }
                        onRefresh={ () => this._refresh() }
                    />
                }
            >
                <List>
                    { this._estudiantes() }
                </List>
                </ScrollView>
            </Content>
        </Container>
        );
    }
}

export default Estudiantes;