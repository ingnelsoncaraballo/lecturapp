import React, { Component, Suspense } from "react";
import { Container, Content, Button, Item, Label, Input, Form, Text } from "native-base";
import { ImageBackground, View, StatusBar, Alert, Keyboard, BackHandler } from "react-native";
import * as axios from 'axios';
import SyncStorage from "sync-storage";
import styles from "./styles/styles";

const fondo = require("../img/fondo.jpg");
const logo = require("../img/logo.png");

class Login extends Component {

    constructor(props){
        super(props);
        this.state = {
            email: "",
            password: ""
        }
    }

    componentDidMount(){
        BackHandler.addEventListener("hardwareBackPress", this.handleBackPress);
        if(global.user.length > 0){ 
            if(global.user.role == 0)
                this.props.navigation.navigate('Home'); 
            else         
                this.props.navigation.navigate('Estudiantes');    
        }
    }

    goToHome(){
        Keyboard.dismiss();
        if(this.state.email != "" && this.state.password != ""){
            axios
            .post(global.url + 'login', {
                email: this.state.email,
                password: this.state.password
            })
            .then(response => {  
                global.user = response.data.user;
                global.readings = response.data.readings;
                SyncStorage.set('login', 'true');   
                SyncStorage.set('id', global.user.id);
                this.setState({ password: "" })
                if(global.user.role == 0)
                    this.props.navigation.navigate('Home'); 
                else         
                    this.props.navigation.navigate('Estudiantes'); 
            })
            .catch(function(error) {          
                if (error.response.status === 404) {
                    Alert.alert('Error', 'El usuario no esta registrado!');
                } else if(error.response.status === 501) {
                    Alert.alert('Error', 'La contraseña es invalida.');
                }
            })
            .finally(() => this.setState({ loading: false, password: "" }));
        }else{
            Alert.alert('Por favor llena todos los campos!');
        }
    }

    goToRegister(){
        this.props.navigation.navigate('Register');
    }
    
    handleBackPress() {
        Alert.alert(
          "Salir",
          "¿Estas seguro de salir de la aplicación?", [
          { text: "Continuar", onPress: () => {  }, style: "cancel" },
          { text: "Salir", onPress: () => BackHandler.exitApp() }
          ],
          { cancelable: false }
        );
        return true;
    }

    render() {
        return (
            <Container style={styles.container}>
                <StatusBar barStyle="light-content" />
                <ImageBackground source={fondo} style={styles.imageContainer}>
                    <Content padder style={{ flex: 1 }}>
                        <View style={styles.logoContainer}>
                            <ImageBackground source={logo} style={styles.logo} />
                        </View>

                        <Form>
                            <Item floatingLabel>
                                <Label>Correo</Label>
                                <Input autoCapitalize="none" keyboardType="email-address" onChangeText={ text => this.setState({ email: text }) } />
                            </Item>
                            <Item floatingLabel last>
                                <Label>Contraseña</Label>
                                <Input secureTextEntry onChangeText={ text => this.setState({ password: text }) } />
                            </Item>
                        </Form>
                        <Button block style={{ margin: 15, marginTop: 50 }} onPress={ () => this.goToHome() }>
                            <Text>Iniciar sesión</Text>
                        </Button>
                        <Text style={{ textAlign: 'center', color: '#00185d' }} onPress={ () => this.goToRegister() }> ¿No estas registrado?</Text>
                        <Text style={{ textAlign: 'center', color: "#00185d" }} onPress={ () => this.goToRegister() }>Registrate</Text>
                    </Content>
                </ImageBackground>
            </Container>
        );
    }
}

export default Login;