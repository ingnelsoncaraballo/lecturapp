import React, { Component } from "react";
import { Image, BackHandler } from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Button,
  Icon,
  Card,
  CardItem,
  Text,
  Thumbnail,
  Left,
  Body,
  Right
} from "native-base";
import styles from "./styles/styles";

const logo = require("../img/logo.png");
const cardImage = require("../img/fondo.jpg");

class Lectura extends Component {

  constructor(props){
    super(props);
    this.state = {

    }
    //this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount(){
      //BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    this.props.navigation.goBack();
    return true;
  }

  lecturas(){
    var lecturaText = [];
    var lecturas = global.readings;
    for (const i in lecturas) {
      lecturaText.push(
        <Card style={styles.mb}>
          <CardItem button={true} onPress={ () => this.props.navigation.navigate('Lectura', { id: i })}> 
            <Left>
              <Thumbnail source={logo} />
              <Body>
                <Text>{ lecturas[i].title }</Text>
                <Text note>{ lecturas[i].bibliography }</Text>
              </Body>
            </Left>
          </CardItem>

          <CardItem cardBody button={true} onPress={ () => this.props.navigation.navigate('Lectura', { id: i })}>
            <Image style={{ resizeMode: "cover", width: null, height: 200, flex: 1 }} source={{ uri: lecturas[i].image }} />
          </CardItem>

          <CardItem style={{ paddingVertical: 0 }} button={true} onPress={ () => this.props.navigation.navigate('Lectura', { id: i })}>
            <Body>
              <Text numberOfLines={4} ellipsizeMode="tail">{ lecturas[i].text }</Text>
            </Body>
          </CardItem>
        </Card>
      )
    }
    return lecturaText;
  }

  render() {
    return (
      <Container style={styles.container}>
        <Header>
          <Left>
            <Button transparent onPress={() => this.props.navigation.goBack()}>
              <Icon name="arrow-back" />
            </Button>
          </Left>
          <Body>
            <Title>Lecturas</Title>
          </Body>
          <Right />
        </Header>

        <Content padder>
          { this.lecturas() }
        </Content>
      </Container>
    );
  }
}

export default Lectura;