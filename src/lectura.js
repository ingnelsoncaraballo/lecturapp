import React, { Component } from "react";
import { Image, Dimensions, BackHandler } from "react-native";
import { Container, Header, Title, Content, Button, Icon, Card, CardItem, Text, Thumbnail, Left, Body, Right } from "native-base";
import HTML from 'react-native-render-html';
import YoutubePlayer from 'react-native-youtube-iframe';
import styles from "./styles/styles";

const logo = require("../img/logo.png");
const cardImage = require("../img/fondo.jpg");

class Lectura extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            play: true,
            id: this.props.navigation.getParam('id'),
            lectura: global.readings[ this.props.navigation.getParam('id') ]
        };
        //this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentDidMount(){
        //BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
  
    handleBackButtonClick() {
      this.props.navigation.goBack();
      return true;
    }

    test(){
        this.setState({
            play: false
        });
        this.props.navigation.navigate('Test', { id: this.state.id })
    }

    videoOrImage(){
        if(this.state.lectura.youtube_link){
            return(
                <YoutubePlayer                            
                    height={200}
                    width={ '100%'}
                    videoId={ this.state.lectura.youtube_link }
                    play={ this.state.play }
                    onChangeState={event => console.log(event)}
                    onReady={() => console.log("ready")}
                    onError={e => console.log(e)}
                    onPlaybackQualityChange={q => console.log(q)}
                    volume={50}
                    playbackRate={1}
                    playerParams={{
                        cc_lang_pref: "es",
                        showClosedCaptions: true
                    }}
                />
            )
        } else {
            return(
                <Image style={{ resizeMode: "cover", width: null, height: 250, flex: 1 }} source={{ uri: this.state.lectura.image }} />
            )
        }
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>{ this.state.lectura.title }</Title>
                    </Body>
                    <Right />
                </Header>

                <Content padder>
                    <Card style={styles.mb}>
                        <CardItem cardBody>
                            { this.videoOrImage() }
                        </CardItem>

                        <CardItem style={{ paddingVertical: 0 }}>
                            <Body>
                                <HTML html={this.state.lectura.text} imagesMaxWidth={Dimensions.get('window').width} />
                            </Body>
                        </CardItem>

                        <CardItem>
                            
                            <Body>
                                <Button block success onPress={ () => this.test() }>
                                    <Icon name="md-checkbox-outline" />
                                    <Text>Realizar Test</Text>
                                </Button>
                            </Body>
                            
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        );
    }
}

export default Lectura;