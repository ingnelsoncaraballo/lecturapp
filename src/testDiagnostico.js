import React, { Component } from "react";
import { View, Alert } from "react-native";
import { Container, Header, Title, Content, Button, Icon, ListItem, Radio, Text, Left, Right, Body, CheckBox} from "native-base";
import styles from "./styles/styles";
import Axios from "axios";

class TestDiagnostico extends Component {
    constructor(props) {
        super(props);
        this.state = {
            lectura: {
                title: "El país de los regalos raros",
                questions: [
                    {
                        question: "1) ¿Cómo se llama el país donde Roberto tuvo el sueño?",
                        answers: [
                            { id: 1, answer: "A.	El país de las maravillas" },
                            { id: 2, answer: "B.	El país de los libros" },
                            { id: 3, answer: "C.	El país de los juguetes" },
                            { id: 4, answer: "D.	El país de los regalos raros" }
                        ]
                    },
                    {
                        question: "2) ¿Qué haría Roberto al ver que despertaba rodeado de pequeños paquetes?",
                        answers:[
                            { id: 5, answer: "A.	Los abría todos para encontrar un teléfono o consola nueva" },
                            { id: 6, answer: "B.	Preguntaba a quién les pertenecía" },
                            { id: 7, answer: "C.	No los destapaba" },
                            { id: 8, answer: "D.	Los ignoraba" }
                        ]
                    },
                    {
                        question: "3)	¿Qué objeto encontró Roberto al abrir la caja?",
                        answers:[
                            { id: 9, answer: "A.	Una consola" },
                            { id: 10, answer: "B.	Un teléfono" },
                            { id: 11, answer: "C.	Un objeto rectangular con dibujos extraños y colores llamativos" },
                            { id: 12, answer: "D.	Un sombrero" }
                        ]
                    },
                    {
                        question: "4)	¿Cuál es el motivo de Roberto colocarse nervioso después de ver un paquete en la mesa?",
                        answers:[
                            { id: 13, answer: "A.	Saber que el sueño que tuvo podría volverse realidad al encontrar el objeto" },
                            { id: 14, answer: "B.	Porqué temía que su mama se fijara que dejo el cuarto desordenado" },
                            { id: 15, answer: "C.	Pensó que podría encontrar una consola nueva en la caja" },
                            { id: 16, answer: "D.   Ninguna de las anteriores" }
                        ]
                    },
                    {
                        question: "5)	¿Cuál es la sorpresa que encontró Roberto luego de abrir la caja?",
                        answers:[
                            { id: 17, answer: "A.	Un computador portátil" },
                            { id: 18, answer: "B.	Unos zapatos nuevos" },
                            { id: 19, answer: "C.	Una enciclopedia" },
                            { id: 20, answer: "D.	Un videojuego" }
                        ]
                    },
                    {
                        question: "6)	¿Qué significa enciclopedia?",
                        answers:[
                            { id: 21, answer: "A.	Es un libro interactivo y educativo donde contiene todas las ciencias como Biología, Medicina, Historia, Etc..." },
                            { id: 22, answer: "B.	Un manual de Videojuego" },
                            { id: 23, answer: "C.	Una Tablet" },
                            { id: 24, answer: "D.	Un celular" }
                        ]
                    },
                    {
                        question: "7)	¿Qué opinas del obsequio que le dio su familia a Roberto?",
                        answers:[
                            { id: 25, answer: "A.	Muy bueno, por medio de la lectura tendrá nuevos conocimientos" },
                            { id: 26, answer: "B.	Aburrido, Roberto quería una nueva consola " },
                            { id: 27, answer: "C.	Me da igual, a Roberto no le interesa leer" },
                            { id: 28, answer: "D.	La opción b y c son correctas" }
                        ]
                    },
                    {
                        question: "8)	¿Crees que es importante saber leer?",
                        answers:[
                            { id: 29, answer: "A.	La lectura es muy importante para cualquier área de la vida ayuda a entender mejor un mensaje y fortalece el conocimiento" },
                            { id: 30, answer: "B.	Leer mucho ocasiona ganas de dormir " },
                            { id: 31, answer: "C.	No me interesa leer" },
                            { id: 32, answer: "D.	La lectura es para los profesores " }
                        ]
                    },
                    {
                        question: "9)	¿Qué piensas de los sueños, pueden volverse realidad?",
                        answers:[
                            { id: 33, answer: "A.	Si, los sueños se repiten a veces en la vida real " },
                            { id: 34, answer: "B.	Es un invento " },
                            { id: 35, answer: "C.	Es un mito" },
                            { id: 36, answer: "D.	Es un cuento" }
                        ]
                    },
                    {
                        question: "10)	 ¿Los videojuegos también sirven para aprender?",
                        answers:[
                            { id: 37, answer: "A.	No, los juegos solo sirven para entretener" },
                            { id: 38, answer: "B.	Los juegos son para olvidarse de las tareas" },
                            { id: 39, answer: "C.	Si, hay muchos juegos que estimulan el conocimiento" },
                            { id: 40, answer: "D.	Ninguna de las anteriores" }
                        ]
                    },
                    
                ]
            },
            respuestas: this.setRespuestas(),
            correctas:[4, 5, 11, 13, 19, 21, 25, 29, 33, 39]
        };
    }

    setRespuestas(){
        var respuestas = [];
        for(var i = 1; i <= 40; i = i + 1){
            respuestas[i] = false;
        }
        return respuestas;
    }

    setRespuesta(id){
        var respuestas = this.state.respuestas;
        respuestas[id] = respuestas[id] == false ? true : false;
        return respuestas;
    }

    toggle(id) {
        this.setState({
            respuestas: this.setRespuesta(id)
        });
    }

    resultado(id){
        return this.state.respuestas[id];
    }

    _test(){
        var question = [];
        var questions = this.state.lectura.questions
        for(var i in questions){
            question.push(
                <View style= { styles.mb20 }>
                    <Text style={{ marginBottom: 20, fontWeight: 'bold' }}>{ questions[i].question }</Text>
                    { this._answers(questions[i].answers) }
                </View>
            );
        }
        return question;
    }

    _answers(answers){
        return (
            answers.map( (item, key) =>
                <ListItem
                    selected={ this.state.respuestas[item.id] }
                    onPress={ () => this.toggle(item.id)  }
                    key={key}
                >
                    <Left>
                        <Text>{item.answer}</Text>
                    </Left>
                    <Right>
                        <Radio
                            selected={ this.state.respuestas[item.id] }
                            onPress={ () => this.toggle(item.id) }
                        />
                    </Right>
                </ListItem>   
            )
        )
    }

    confirmarRespuestas(){
        Alert.alert(
            "Confirmar",
            "¿Estas seguro de enviar estas repuestas?", [
                { text: "No", onPress: () => {  }, style: "cancel" },
                { text: "Si", onPress: () => this.obtenerResultado() }
            ],
            { cancelable: false }
          );
    }

    obtenerResultado(){
        var porcentaje = 0;
        for(var i in this.state.correctas){
            if(this.state.respuestas[this.state.correctas[i]] == true){
                porcentaje = parseInt(porcentaje) + 10;
            }
        }
        Alert.alert("puntaje: " + porcentaje);
        this.enviarRespuesta(porcentaje);
    }

    enviarRespuesta(porcentaje){
        Axios.post(
            global.url + 'porcentaje',
            {
                user_id: global.user.id,
                porcentaje:porcentaje
            }
        )
        .then(response => {
            global.user = response.data.user;
            global.readings = response.data.readings;
            Alert.alert("Respuestas enviadas", "Respuestas enviadas con exito, puedes revisar tus estadisticas",
            [{ text: "ok", onPress: () =>  this.props.navigation.navigate('Home') }],
            { cancelable: false });
        })
        .catch( function(error){
            console.log(error);
            Alert.alert("Error enviando las respuestas");
        });
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>{ this.state.lectura.title }</Title>
                    </Body>
                    <Right />
                </Header>

                <Content padder>
                    { this._test() }
                    <Button primary block onPress={ () => this.confirmarRespuestas() }>
                        <Text>Enviar respuestas</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default TestDiagnostico;