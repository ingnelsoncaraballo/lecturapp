import React, { Component } from "react";
import { Dimensions } from "react-native";
import { Container, Header, Title, Content, Text, Button, Icon, Left, Right, Body } from "native-base";
import { LineChart, BarChart, PieChart, ProgressChart, ContributionGraph, StackedBarChart } from "react-native-chart-kit";

import Styles from "./styles/styles";
import Axios from "axios";

const screenWidth = Dimensions.get("window").width;

const chartConfig = {
    backgroundGradientFrom: "#417dc1",
    backgroundGradientFromOpacity: 0.1,
    backgroundGradientTo: "#0071bc",
    backgroundGradientToOpacity: 1,
    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
    strokeWidth: 2,
    barPercentage: 1,
    useShadowColorFromDataset: true
};

class Estadistica extends Component {

    constructor(props){
        super(props);
        this.state = {
            data: {
                labels: ["Lectura 1", "Lectura 2", "Lectura 3"],
                datasets: [{ data: [ 5, 4, 5 ] }]
            },
            animo:  "Datos de ejemplo, tus estadisticas estarán disponibles cuando empieces los test de tu nivel, animo!"
        };
    }

    componentDidMount(){
        Axios.post(global.url + 'tests', {
            id: global.user.id
        })
        .then(response => {
            var lecturas = response.data;
            var lectura = [];
            var promedio = [];
            var item = 0;
            for(var i in lecturas){
                if(this.find(lectura, lecturas[i].readings)){
                    lectura[item] =  lecturas[i].readings;
                    promedio[item] = parseFloat(lecturas[i].promedio);
                    item = item + 1;
                }
            }

            if(lectura.length > 0){
                this.setState({
                    data: {
                        labels: lectura,
                        datasets: [{ data: promedio }]
                    },
                    animo: "Tus esfuerzos se han multiplicado, Excelente! Estos son tus resultados:",
                });
            }
        })
        .catch(function(error){
            console.log(error);
        });
    }

    find(lectura, texto){
        for(var it in lectura){
            if(lectura[it] == texto)
                return false;
        }
        return true;
    }

    render() {
        return (
            <Container style={ Styles.container }>
                <Header>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Estadisticas</Title>
                    </Body>
                    <Right />
                </Header>

                <Content padder>
                    <Text style={ Styles.mb10 }>{ this.state.animo }</Text>
                    <BarChart 
                        data={this.state.data}
                        width={screenWidth - 20}
                        height={650}
                        yAxisLabel=""
                        chartConfig={chartConfig}
                        verticalLabelRotation={50}
                        fromZero={true}
                        showBarTops={true}
                        showValuesOnTopOfBars={true}
                    />
                    <LineChart
                        data = { this.state.data }
                        width = { screenWidth - 20 }
                        height = {250}
                        yAxisLabel = ""
                        yAxisSuffix = ""
                        yAxisInterval = {1}
                        chartConfig={{
                            backgroundColor: "#e26a00",
                            backgroundGradientFrom: "#fb8c00",
                            backgroundGradientTo: "#ffa726",
                            decimalPlaces: 1,
                            yAxisLabel: 'Tus notas',
                            color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                            style: {
                                borderRadius: 5
                            },
                            propsForDots: {
                                r: "5",
                                strokeWidth: "2",
                                stroke: "#ffa726"
                            }
                        }}
                        bezier
                        style={{
                            marginVertical: 8,
                            borderRadius: 5
                        }}
                        fromZero={true}
                    />
                </Content>
            </Container>
        );
    }
}

export default Estadistica;