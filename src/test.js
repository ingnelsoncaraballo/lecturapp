import React, { Component } from "react";
import { View, Alert } from "react-native";
import { Container, Header, Title, Content, Button, Icon, ListItem, Radio, Text, Left, Right, Body, CheckBox} from "native-base";
import styles from "./styles/styles";
import Axios from "axios";

class Test extends Component {
    constructor(props) {
        super(props);
        const reading_id =  this.props.navigation.getParam('id', 0);
        this.state = {
            lectura: global.readings[ reading_id ],
            respuestas: this.setRespuestas( global.readings[ reading_id ])
        };
    }

    setRespuestas(reading){
        var respuestas = [];
        for(var q in reading.questions){           
            for(var a in reading.questions[q].answers){
                respuestas[reading.questions[q].answers[a].id] = false;
            }            
        }
        return respuestas;
    }

    setRespuesta(id){
        var respuestas = this.state.respuestas;
        var reading = this.state.lectura;
        for(var q in reading.questions){            
            for(var a in reading.questions[q].answers){
                if(reading.questions[q].answers[a].id == id)
                    respuestas[id] = respuestas[id] == false ? true : false;
            }            
        }
        return respuestas;
    }

    toggle(id) {
        this.setState({
            respuestas: this.setRespuesta(id)
        });
    }

    resultado(id){
        return this.state.respuestas[id];
    }

    _test(){
        var question = [];
        var questions = this.state.lectura.questions
        for(var i in questions){           
            question.push(
                <View style={{ marginBottom: 50 }}>
                    <Text style={{ marginBottom: 20, fontWeight: 'bold' }}>{parseInt(i) + 1}) { questions[i].question }</Text>
                    { this._answers(questions[i]) }
                </View>
            );            
        }
        return question;
    }

    _answers(question){
        return (
            question.answers.map( (item, key) =>
                <ListItem
                    selected={ this.state.respuestas[item.id] }
                    onPress={ () => this.toggle(item.id)  }
                >
                    <Left>
                        <Text>{item.answer}</Text>
                    </Left>
                    <Right>
                        <Radio
                            selected={ this.state.respuestas[item.id] }
                            onPress={ () => this.toggle(item.id) }
                        />
                    </Right>
                </ListItem>   
            )
        )
    }

    confirmarRespuestas(){
        Alert.alert(
            "Confirmar",
            "¿Estas seguro de enviar estas repuestas?", [
                { text: "No", onPress: () => {  }, style: "cancel" },
                { text: "Si", onPress: () => this.enviarRespuestas() }
            ],
            { cancelable: false }
          );
    }

    enviarRespuestas(){
        Axios.post(
            global.url + 'respuestas',
            {
                user_id: global.user.id,
                lectura_id: this.state.lectura.id,
                respuestas: this.state.respuestas
            }
        )
        .then(response => {
            Alert.alert("Respuestas enviadas", "Respuestas enviadas con exito, puedes revisar tus estadisticas",
            [{ text: "ok", onPress: () =>  this.props.navigation.navigate('Lecturas') }],
            { cancelable: false });
        })
        .catch( function(error){
            console.log(error);
            Alert.alert("Error enviando las respuestas");
        });
    }

    render() {
        return (
            <Container style={styles.container}>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>{ this.state.lectura.title }</Title>
                    </Body>
                    <Right />
                </Header>

                <Content padder>
                    { this._test() }
                    <Button primary block onPress={ () => this.confirmarRespuestas() }>
                        <Text>Enviar respuestas</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}

export default Test;