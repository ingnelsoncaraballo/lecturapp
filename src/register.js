import React, { Component } from "react";
import { Container, Header, Title, Content, Button, Item, Label, Input, Body, Left, Right, Icon, Form, Text, H1, Picker } from "native-base";
import { Alert, ImageBackground, View, StatusBar, Keyboard } from "react-native";
import SyncStorage from "sync-storage";
import * as axios from 'axios';
import styles from "./styles/styles";

const fondo = require("../img/fondo.jpg");
const logo = require("../img/logo.png");
let lista;

class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
          role: "0",
          name: "",
          lastname: "",
          email: "",
          password: "",
          password2: "",
          age: "",
          institutions: "",
          institution_id: 0
        };
        this.cargarInstituciones();
    }

    componentDidMount(){
        //this.cargarInstituciones();
    }

    cargarInstituciones(){
        axios.get(global.url + 'institutions')
        .then((response) => {
            this.setState({ institutions: Object.entries(response.data) });
        })
        .catch(function (error) {
            Alert.alert('Intente más tarde, no se descargaron las Instituciones');
        });
    }

    onValueChange(value: string) {
        this.setState({
          role: value
        });
    }

    onValueChangeInstitution(value) {
        this.setState({
          institution_id: value
        });
    }

    goToHome(){
        this.props.navigation.navigate('Home');
    }

    goToLogin(){
        this.props.navigation.navigate('Login');
    }

    registrarse(){
        Keyboard.dismiss();
        if(this.state.institution_id != 0 && this.state.name != '' && this.state.lastname != '' && this.state.email != '' && this.state.age != '' && this.state.password != ''){
            if(this.state.password != this.state.password2){
                Alert.alert('Las contraseñas no coinciden');
            } else {
                this.setState({ loading: true });
                axios
                .post(global.url + 'registro', {
                    institution_id: this.state.institution_id,
                    role: this.state.role,
                    name: this.state.name,
                    lastname: this.state.lastname,
                    email: this.state.email,
                    password: this.state.password,
                    age: this.state.age
                })
                .then(response => {  
                    global.user = response.data.user;
                    global.readings = response.data.readings;
                    SyncStorage.set('login', 'true');   
                    SyncStorage.set('id', global.user.id);
                    this.continuar();
                })
                .catch(function(error) {          
                    if (error.response.status === 404) {
                        Alert.alert('Error', 'Este correo ya esta registrado.');
                    } else if(error.response.status === 501) {
                        Alert.alert('Error', 'Este correo es invalido.');
                    }
                })
                .finally(() => this.setState({ loading: false }));
            }            
        } else {
            Alert.alert('Por favor rellene todos los campos');
        }
    }

    continuar(){
        if(global.user.role == 0){
            Alert.alert("Cuenta creada!", "Su cuenta ha sido creada con éxito, ahora realizaremos un test diagnostico para evaluar tu nivel. Éxitos!!", [
                    { text: "Realizar", onPress: () => this.props.navigation.navigate('Diagnostico') },
                    { text: "Más tarde", onPress: () => this.masTarde() }
                ],
                { cancelable: false }
            ); 
        }else {       
            Alert.alert("Cuenta creada!", "Su cuenta ha sido creada con éxito, muchas gracias!", [
                    { text: "Continuar", onPress: () =>  this.props.navigation.navigate('Estudiantes') }
                ],
                { cancelable: false }
            );
        }
    }

    masTarde(){
        SyncStorage.set('login', 'false');
        this.props.navigation.navigate('Login');
    }
    
    render() {

        if(this.state.institutions != ""){
            lista = this.state.institutions.map( (s) => {
                return <Picker.Item value={s[0]} label={s[1]} />
            });
        }

        return (
            <Container style={styles.container}>
                <StatusBar barStyle="light-content" />
                <ImageBackground source={fondo} style={styles.imageContainer}>
                    <Content padder style={{ flex: 1 }}>
                        <H1 style={[ styles.blue, styles.mt20, styles.mb20, { textAlign: 'center' }]}>Registro</H1>

                        <Form>
                            <Picker
                                mode="dropdown"
                                iosHeader="Selecciona tu rol"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width: '95%', marginLeft: 10 }}
                                selectedValue={this.state.institution_id}
                                onValueChange={this.onValueChangeInstitution.bind(this)}
                                >
                                    <Picker.Item label="Seleccione su Colegio" value="0" />
                                    { lista }
                                    
                            </Picker>
                            <Picker
                                mode="dropdown"
                                iosHeader="Selecciona tu rol"
                                iosIcon={<Icon name="arrow-down" />}
                                style={{ width: '95%', marginLeft: 10, marginBottom: 0 }}
                                selectedValue={this.state.role}
                                onValueChange={this.onValueChange.bind(this)}
                                >
                                <Picker.Item label="Estudiante" value="0" />
                                <Picker.Item label="Docente" value="1" />
                            </Picker>

                            <Item floatingLabel style={{ marginTop: 0 }}>
                                <Label>Nombre</Label>
                                <Input onChangeText={ text => this.setState({ name: text }) }  />
                            </Item>
                            <Item floatingLabel>
                                <Label>Apellidos</Label>
                                <Input onChangeText={ text => this.setState({ lastname: text }) }  />
                            </Item>
                            <Item floatingLabel>
                                <Label>Email</Label>
                                <Input autoCapitalize={"none"} keyboardType="email-address" onChangeText={ text => this.setState({ email: text }) }  />
                            </Item>
                            <Item floatingLabel>
                                <Label>Contraseña</Label>
                                <Input secureTextEntry onChangeText={ text => this.setState({ password: text }) }  />
                            </Item>
                            <Item floatingLabel>
                                <Label>Confirmar contraseña</Label>
                                <Input secureTextEntry onChangeText={ text => this.setState({ password2: text }) }  />
                            </Item>
                            <Item floatingLabel>
                                <Label>Edad</Label>
                                <Input keyboardType="numeric" onChangeText={ text => this.setState({ age: text }) }  />
                            </Item>
                        </Form>
                        <Button block style={{ margin: 15, marginTop: 50 }} onPress={ () => this.registrarse() }>
                            <Text>Registrarse</Text>
                        </Button>
                        <Text style={{ textAlign: 'center', color: '#00185d' }} onPress={ () => this.goToLogin() }> ¿Ya tienes cuenta?</Text>
                        <Text style={{ textAlign: 'center', color: "#00185d" }} onPress={ () => this.goToLogin() }>Inicia sesión</Text>
                    </Content>
                </ImageBackground>
            </Container>
        );
    }
}

export default Register;