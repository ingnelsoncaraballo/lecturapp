import React, { Component } from "react";
import { View, Alert, ImageBackground } from "react-native";
import { Container, Header, Title, Content, Button, Icon, ListItem, Radio, Text, Left, Right, Body, CheckBox, Card, CardItem} from "native-base";
import styles from "./styles/styles";
import Axios from "axios";

const fondo = require("../img/fondo2.png");

class Test extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reading:  this.props.navigation.getParam('title', ''),
            respuestas: this.props.navigation.getParam('respuestas', '')
        };
    }

    calificacion(text){
        if(text == 'Y'){
            return (
                <Button transparent success>
                    <Icon active name="checkmark" />
                    <Text>Correcto</Text>
                </Button>
            )
        } else {
            return (
                <Button transparent danger>
                    <Icon active name="close" />
                    <Text>Incorrecto</Text>
                </Button>
            )
        }
    }

    _answers(){
        var respuesta = [];
        var item = 0;
        var respuestas = this.state.respuestas;
        for(var it in respuestas){
            if(respuestas[it].readings == this.state.reading){
                respuesta[item] = respuestas[it];
                item = item + 1;
            }
        }

        return (
            respuesta.map( (item, key) =>
                <Card key={ key }>
                    <CardItem header bordered>
                        <Text>{ key + 1 }) { item.question.question }</Text>
                    </CardItem>
                    <CardItem bordered>
                        <Body>
                            <Text>
                                Tu Respuesta: { item.question.answers_student[0].answer }
                            </Text>
                        </Body>
                    </CardItem>
                    <CardItem>
                        <Left>
                            <Text>Estado:</Text>
                        </Left>                        
                        <Right>
                            { this.calificacion(item.question.answers_student[0].correct) }
                        </Right>
                    </CardItem>
                </Card> 
            )
        )
    }

    render() {
        return (
            <Container style={styles.container}>
                <ImageBackground source={fondo} style={styles.imageContainer}>
                    <Header>
                        <Left>
                            <Button transparent onPress={() => this.props.navigation.goBack()}>
                                <Icon name="arrow-back" />
                            </Button>
                        </Left>
                        <Body>
                            <Title>{ this.state.reading }</Title>
                        </Body>
                        <Right />
                    </Header>

                    <Content padder>
                    { this._answers() }
                    </Content>
                </ImageBackground> 
            </Container>
        );
    }
}

export default Test;