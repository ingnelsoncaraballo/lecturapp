import React from "react";
import { Root } from "native-base";
import { createDrawerNavigator } from "react-navigation-drawer";
import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";

//Views
import Splash from "./splash";
import SideBar from "./sidebar";
import Login from "./login";
import Register from "./register";
import Home from "./home";
import Lecturas from "./lecturas";
import Lectura from "./lectura";
import Test from "./test";
import Tests from "./tests";
import Respuestas from "./respuestas";
import Estadistica from "./estadistica";
import Diagnostico from "./diagnostico";
import TestDiagnostico from "./testDiagnostico";
import Estudiantes from "./estudiantes";

import Perfil from "./perfil";
import Acerca from "./acerca";

const Drawer = createDrawerNavigator(
  {
      Home: { screen: Home },
      Splash: { screen: Splash },
      Login: { screen: Login },
      Register: { screen: Register },
      Estudiantes: { screen: Estudiantes }
  },
  {
      initialRouteName: "Splash",
      contentOptions: {
            activeTintColor: "#e91e63"
        },
        contentComponent: props => <SideBar {...props} />
  }
);

const AppNavigator = createStackNavigator(
    {
        Drawer: { screen: Drawer },
        Lectura: { screen: Lectura },
        Lecturas: {screen: Lecturas },
        Test: { screen: Test },
        Tests: { screen: Tests },
        Respuestas: { screen: Respuestas },
        Estadistica: { screen: Estadistica },
        Perfil: { screen: Perfil },   
        Acerca: { screen: Acerca },
        Diagnostico: { screen: Diagnostico },
        TestDiagnostico: { screen: TestDiagnostico },
    },
    {
        initialRouteName: "Drawer",
        headerMode: "none"
    }
);

const AppContainer = createAppContainer(AppNavigator);

global.url = 'https://lecturapp.digital/api/public/';
global.asset = 'https://lecturapp.digital/';
global.vista = 'Inicio';
global.user = [];
global.readings = [];

export default () =>
    <Root>
        <AppContainer />
    </Root>;