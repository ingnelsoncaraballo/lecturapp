import React, { Component } from "react";
import { Container } from "native-base";
import { ImageBackground, StatusBar } from "react-native";
import SyncStorage from "sync-storage";
import styles from "./styles/styles";
import Axios from "axios";

const fondo = require("../img/logo2.png");

class Splash extends Component {

    goToHome(){

        const login = SyncStorage.get('login');

        if(login == 'true'){
            const id = SyncStorage.get('id');
            Axios.post(global.url + 'user', {
                id: id
            })
            .then(response => {
                global.user = response.data.user;
                global.readings = response.data.readings;
                SyncStorage.set('login', 'true');   
                SyncStorage.set('id', global.user.id);
                if(global.user.role == 0)
                    this.props.navigation.navigate('Home'); 
                else         
                    this.props.navigation.navigate('Estudiantes');
            })
            .catch(function(error){
                this.props.navigation.navigate('Login');
            });         
        } else {
            this.props.navigation.navigate('Login');
        }        
    }

    componentDidMount() {
        setTimeout( () => {
            this.goToHome();
        }, 3000 );
    }
    
    render() {
        return (
            <Container style={styles.container}>
                <StatusBar barStyle="light-content" />
                <ImageBackground source={fondo} style={{ marginTop: 60, flex: 1, width: 350, height: 350 }}>
                </ImageBackground>
            </Container>
        );
    }
}

export default Splash;