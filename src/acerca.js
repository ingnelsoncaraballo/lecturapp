import React, { Component } from 'react';
import { View, Image, ImageBackground } from 'react-native';
import { Container, Header, Body, Left, Icon, Button, Title, Right, H2 } from 'native-base';
import { version } from '../package.json';
import styles from "./styles/styles";


const splashscreen = require("../img/fondo.jpg");
const logo = require("../img/logo.png");

class Acerca extends Component {

    constructor(props) {
        super(props);
        this.state = {
            version: version
        };
    }

    render() {
        return (
            <Container>
                <Header>
                    <Left>
                        <Button transparent onPress={() => this.props.navigation.goBack()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Acerca de la app</Title>
                    </Body>
                    <Right />
                </Header>
                <ImageBackground source={splashscreen} style={{width: '100%', height: '100%'}}>
                    <View style={ styles.logoContainer }>          
                        <Image source={logo} style={ styles.logo } />   
                    </View>
                    <View style={ styles.center }>          
                        <H2>Version { this.state.version }</H2>          
                    </View>
                </ImageBackground>
            </Container>
        );
    }
}

export default Acerca;