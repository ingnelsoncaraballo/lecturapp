import React, { Component } from "react";
import { Image } from "react-native";
import { Content, Text, List, ListItem, Icon, Container, Left, Right, Badge } from "native-base";
import SyncStorage from "sync-storage";
import styles from "./styles/styles";

const fondo = require("../img/fondo.jpg");
const logo = require("../img/logo.png");
//const drawerImage = require("../../../assets/logo-kitchen-sink.png");
const datas = [
    {
        name: "Inicio",
        route: "Home",
        icon: "home",
        bg: "#C5F442"
    },
    {
        name: "Perfil",
        route: "Perfil",
        icon: "md-person",
        bg: "#477EEA"
    },
    {
        name: "Acerca",
        route: "Acerca",
        icon: "md-tablet-portrait",
        bg: "#477EEA"
    },
    {
        name: "Salir",
        route: "Salir",
        icon: "md-exit",
        bg: "#477EEA"
    }
];

class SideBar extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shadowOffsetWidth: 1,
            shadowRadius: 4
        };
    }

    middleware(route){
        if(route == 'Salir'){
            SyncStorage.set('login', 'false');
            this.props.navigation.navigate('Login');
        } else {
            this.props.navigation.closeDrawer();
            this.props.navigation.navigate(route);
        }
    }

    render() {
        return (
            <Container>
                <Content
                    bounces={false}
                    style={{ flex: 1, backgroundColor: "#fff", top: -1 }}
                >
                    <Image source={fondo} style={styles.drawerCover} />
                    <Image square style={[ styles.drawerImage, { width: 150, height: 150}]} source={logo} />

                    <List
                        dataArray={datas}
                        renderRow={data =>
                            <ListItem
                                button
                                noBorder
                                onPress={() => this.middleware(data.route)}
                            >
                                <Left>
                                    <Icon
                                        active
                                        name={data.icon}
                                        style={{ color: "#777", fontSize: 26, width: 30 }}
                                    />
                                    <Text style={styles.menuText}>
                                        {data.name}
                                    </Text>
                                </Left>
                                {data.types &&
                                    <Right style={{ flex: 1 }}>
                                        <Badge
                                            style={{
                                                borderRadius: 3,
                                                height: 25,
                                                width: 72,
                                                backgroundColor: data.bg
                                            }}
                                        >
                                            <Text
                                                style={styles.badgeText}
                                            >{`${data.types} Types`}</Text>
                                        </Badge>
                                    </Right>}
                            </ListItem>}
                    />
                </Content>
            </Container>
        );
    }
}

export default SideBar;