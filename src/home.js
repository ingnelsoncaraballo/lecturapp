import React, { Component } from "react";
import { View, ImageBackground, Image, BackHandler, Alert,  Modal, Dimensions } from "react-native";
import { Container, Header, Title, Content, Button, Icon, Left, Right, Body, Text, Card, CardItem } from "native-base";
import Sound from "react-native-sound";
import styles from "./styles/styles";
import HTML from "react-native-render-html";

const fondo = require("../img/fondo2.png");
const lectura = require("../img/lectura.jpg");
const test = require("../img/test.jpg");
const est = require("../img/est.jpg");

const swoosh = new Sound('http://lecturapp.digital/sounds/swoosh.mp3', null, (error) => {});

class Home extends Component {

    constructor(props){        
        super(props);
        this.state = {
            modalVisible: true,
        }
        this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    }

    componentDidMount(){
        if(global.user.nivel == 0){
            this.hacerTest();
        }
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    lecturas() {
        swoosh.play();
        this.props.navigation.navigate('Lecturas');
    }

    testDiagnostico(){
        swoosh.play();
        this.props.navigation.navigate('Diagnostico');
    }

    tests() {
        swoosh.play();
        this.props.navigation.navigate('Tests');
    }

    estadistica() {
        swoosh.play();
        this.props.navigation.navigate('Estadistica');
    }

    handleBackButtonClick() {
        Alert.alert(
          "Salir",
          "¿Estas seguro de salir de la aplicación?", [
          { text: "No, Seguir", onPress: () => {  }, style: "cancel" },
          { text: "Si, Salir", onPress: () => BackHandler.exitApp() }
          ],
          { cancelable: false }
        );
        return true;
    }

    hacerTest(){
        Alert.alert(
            "Test Diagnostico",
            "Bienvenidos, primeramente realizaremos un test diagnostico para evaluar tu nivel. Éxitos!", [
                { text: "Realizar", onPress: () => this.testDiagnostico() }
            ],
            { cancelable: false }
          );
    }

    render() {
        return (
            <Container style={styles.container}>
                <ImageBackground source={fondo} style={styles.imageContainer}>
                    <Header>
                        <Left>
                            <Button transparent onPress={() => this.props.navigation.openDrawer()}>
                                <Icon name="menu" />
                            </Button>
                        </Left>
                        <Body >
                            <Title>LecturApp</Title>
                        </Body>
                        <Right>
                        <Button transparent onPress={ () => this.props.navigation.navigate('Perfil') }>
                                <Icon name="md-person" />
                                <Text>Mi Perfil</Text>
                            </Button>
                        </Right>
                    </Header>

                    <Content padder>
                        <Card>           
                            <CardItem cardBody button onPress={() => this.lecturas()}>
                                <Image source={lectura} style={{height: 150, width: '100%', flex: 1}}/>
                            </CardItem>
                        </Card>
                        <Card>           
                            <CardItem cardBody button onPress={() => this.tests()}>
                                <Image source={test} style={{height: 150, width: '100%', flex: 1}}/>
                            </CardItem>
                        </Card>
                        <Card>           
                            <CardItem cardBody button onPress={() => this.estadistica()}>
                                <Image source={est} style={{height: 150, width: '100%', flex: 1}}/>
                            </CardItem>
                        </Card>
                    </Content>
                </ImageBackground>
            </Container>
        );
    }
}

export default Home;